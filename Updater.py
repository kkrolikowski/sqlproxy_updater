import requests
import json
import time
import MySQLdb

class ETCDClass:
    def __init__(self, etcdAddr, clName):
        self.etcdAddr = etcdAddr
        self.clName = clName
        self.nodes = []
    def __del__(self):
        self.nodes = []
    def read(self):
        try:
            etcd = requests.get("http://" + self.etcdAddr + "/v2/keys/pxc-cluster/" + self.clName, timeout=5)
        except requests.exceptions.Timeout:
            print("ETCD connection timeout. Check if ETCD is synchronized...")
        except requests.exceptions.ConnectionError:
            print("ETCD temporarily unavailable. Backoff engaged. (10s)")
            time.sleep(10)
        else:
            if etcd.status_code == 200:
                etcd_json = json.loads(etcd.text)
                if 'nodes' not in etcd_json['node']:
                    print("SQL nodes not present in ETCD")
                    return None
                for sqlnodes in etcd_json['node']['nodes']:
                    hosts = sqlnodes['key'].split('/')
                    self.nodes.append(hosts[3])
            else:
                print("ETCD HTTP Response code is invalid: " + str(etcd.status_code))
        return self.nodes

class SQLProxy:
    def __init__(self, proxy_host, proxy_admin, proxy_admin_pass):
        try:
            self.db = MySQLdb.connect(host=proxy_host,
                                port=6032,
                                user=proxy_admin,
                                passwd=proxy_admin_pass)
        except MySQLdb.Error:
            return None
        else:
            self.cur = self.db.cursor()
    
    def __del__(self):
        if self.cur:
            self.cur.close()
        if self.db:
            self.db.close()

    def insert(self, nodelist):
        for node in nodelist:
            self.cur.execute("SELECT * FROM mysql_servers WHERE hostname = \'" + node + "\'")
        
            nodeCount = self.cur.rowcount
            if self.cur.rowcount is 0:
                print("Adding " + node + " to proxysql")
                self.cur.execute("INSERT INTO mysql_servers (hostgroup_id, hostname, port, max_replication_lag) VALUES (0, \'" + node + "\', 3306, 20)")
                self.cur.execute("LOAD MYSQL SERVERS TO RUNTIME")
                self.cur.execute("SAVE MYSQL SERVERS TO DISK")
    def count(self):
        self.cur.execute("SELECT hostname FROM runtime_mysql_servers where status = 'ONLINE'")
        return self.cur.rowcount