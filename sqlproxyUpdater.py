#!/usr/bin/python

import os
import time
import datetime
from Updater import *


def main():
    date = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    print(date + " Starting updater...")

    while True:
        etcd_sqlnodes = []
        proxydb_sqlnodes = []
        nodeCount = 0
        date = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
        
        if os.getenv('DISCOVERY_SERVICE') is None:
            print(date + " DISCOVERY_SERVICE variable is not set")
            continue
        if os.getenv('CLUSTER_NAME') is None:
            print(date + " CLUSTER_NAME variable is not set")
            continue
        if os.getenv('SQLPROXY_HOST') is None:
            print(date + " SQLPROXY_HOST variable is not set")
            continue
        if os.getenv('PROXY_ADMIN_USER') is None:
            print(date + " PROXY_ADMIN_USER variable is not set")
            continue
        if os.getenv('PROXY_ADMIN_PASS') is None:
            print(date + " PROXY_ADMIN_PASS variable is not set")
            continue
        
        etcd = ETCDClass(os.environ['DISCOVERY_SERVICE'], os.environ['CLUSTER_NAME'])
        print(date + " Reading data from etcd...")
        etcd_sqlnodes = etcd.read()

        if not etcd_sqlnodes:
            time.sleep(2)
            continue

        proxydb = SQLProxy(os.environ['SQLPROXY_HOST'],os.environ['PROXY_ADMIN_USER'], os.environ['PROXY_ADMIN_PASS'])
        
        if proxydb == None:
            print(date + " Database connection error, reconnecting")
            time.sleep(2)
            continue
        if proxydb == None:
            print(date + " ProxySQL database not ready, retrying ...")
            time.sleep(2)
            continue
    
        proxydb.insert(etcd_sqlnodes)
        nodeCount = proxydb.count()
        
        print(date + " ProxySQL is up to date. ProxySQL DB: " + str(nodeCount) + ", ETDC: " + str(len(etcd_sqlnodes)))

        time.sleep(2)

if __name__ == '__main__':
    main()